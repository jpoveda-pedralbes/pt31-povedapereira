package cat.inspedralbes.m8.pt31povedapereira;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class ScreenRender implements Screen{

	private ScreenController nivell;
	private OrthographicCamera camera;
	private Viewport viewport;
	private SpriteBatch batch;
	private Texture bgTexture;

	public ScreenRender (ScreenController nivell) {
		this.nivell = nivell;
		bgTexture = new Texture(Gdx.files.internal("levelBg.png"));
		
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, Constants.ALT_PANTALLA, Constants.AMPLE_PANTALLA);
		viewport = new FitViewport(Constants.ALT_PANTALLA, Constants.AMPLE_PANTALLA, camera);
		camera.update();
	}
	
	public void render() {
		renderObjectesPantalla();
	}
	
	private void renderObjectesPantalla() {
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		batch.draw(bgTexture, 0, 0, Constants.ALT_PANTALLA, Constants.AMPLE_PANTALLA);
		for (Bloc bloc : nivell.blocs) {
			bloc.dibuixarse(batch);
		}
		nivell.jugador.dibuixarse(batch);
		nivell.pilota.dibuixarse(batch);
		batch.end();
	}

	@Override
	public void dispose() {
		batch.dispose();
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

}
