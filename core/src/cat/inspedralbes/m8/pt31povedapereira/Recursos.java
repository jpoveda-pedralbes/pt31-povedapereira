package cat.inspedralbes.m8.pt31povedapereira;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Disposable;

public class Recursos implements Disposable {
	private Texture jugador, pilota, bloc;
	public static final Recursos instance = new Recursos();

	private Recursos () {
		jugador = new Texture(Gdx.files.internal("player/player.png"));
		bloc  = new Texture(Gdx.files.internal("bricks/orange.png"));
		pilota   = new Texture(Gdx.files.internal("balls/red.png"));
	}
	
	public Texture getJugador () {
		return jugador;
	}
	
	public Texture getPilota () {
		return pilota;
	}
	
	public Texture getBloc () {
		return bloc;
	}

	@Override
	public void dispose() {
		jugador.dispose();
		pilota.dispose();
		bloc.dispose();
	}
}
