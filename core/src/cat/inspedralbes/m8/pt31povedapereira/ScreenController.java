package cat.inspedralbes.m8.pt31povedapereira;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public class ScreenController extends InputAdapter implements Screen {

	public Array<Bloc>blocs;
	public Jugador jugador;
	public Pilota pilota;
	private float velocitatPilotaX;
	private float velocitatPilotaY;
	
	public ScreenController () {
		Gdx.input.setInputProcessor(this);
		jugador = new Jugador(50, 30);
		pilota = new Pilota(240, 400);
		velocitatPilotaX = MathUtils.random(Constants.VELOCITAT_PILOTA*-1, Constants.VELOCITAT_PILOTA);
		velocitatPilotaY = Constants.VELOCITAT_PILOTA;
		pilota.estableixVelocitat(velocitatPilotaX, -velocitatPilotaY);
		dibuixarBlocs();
	}
	
	private void dibuixarBlocs() {
		blocs = new Array<Bloc>();
		int y = Constants.AMPLE_PANTALLA-100;
		for (int j =0; j < 8; j ++) {
			int x = 50;
			for (int i = 0; i < 15; i++){
				Bloc bloc = new Bloc(x, y);
				blocs.add(bloc);
				x += Constants.AMPLE_BLOC;
			}
			y -= Constants.ALT_BLOC+5;
		}
	}
	
	public void actualitzar (float delta) {
		gestioInputJugador(delta);
		pilota.estableixVelocitat(velocitatPilotaX, velocitatPilotaY);
		velocitatPilota();
		jugador.actualitzarse(delta);
		pilota.actualitzarse(delta);
		comprobarColisioPilotaMur();
		comprobarColisioPilotaJugador();
		comprobarColisioPilotaBloc();
	}
	
	private void velocitatPilota() {
		if (velocitatPilotaX < 0 && velocitatPilotaX < (Constants.VELOCITAT_PILOTA*-1))
			velocitatPilotaX = (Constants.VELOCITAT_PILOTA*-1);
		if (velocitatPilotaX > 0 && velocitatPilotaX > Constants.VELOCITAT_PILOTA)
			velocitatPilotaX = Constants.VELOCITAT_PILOTA;
		if (velocitatPilotaY < 0 && velocitatPilotaY < (Constants.VELOCITAT_PILOTA*-1))
			velocitatPilotaY = (Constants.VELOCITAT_PILOTA*-1);
		if (velocitatPilotaY > 0 && velocitatPilotaY > Constants.VELOCITAT_PILOTA)
			velocitatPilotaY = Constants.VELOCITAT_PILOTA;
	}
	
	private void comprobarColisioPilotaMur () {
		if (pilota.getLimits().x < 14) {
			velocitatPilotaX *= -1;
		}
		if (pilota.getLimits().x > Constants.ALT_PANTALLA-30) {
			velocitatPilotaX *= -1;
		}
		if (pilota.getLimits().y < 0) {
			velocitatPilotaY *= -1;
		}
		if (pilota.getLimits().y > Constants.AMPLE_PANTALLA-40) {
			velocitatPilotaY *= -1;
		}
	}
	
	private void comprobarColisioPilotaJugador () {
		if (Intersector.overlaps(pilota.getLimits(), jugador.limit2)) {
			velocitatPilotaY *= -1;
		}
		if (Intersector.overlaps(pilota.getLimits(), jugador.limit1)) {
			velocitatPilotaY *= -1;
			velocitatPilotaX -= (Constants.VELOCITAT_PILOTA/2);
		}
		if (Intersector.overlaps(pilota.getLimits(), jugador.limit3)) {
			velocitatPilotaY *= -1;
			velocitatPilotaX += (Constants.VELOCITAT_PILOTA/2);
		}
	}
	
	private void comprobarColisioPilotaBloc () {
		for (Bloc bloc : blocs) {
			if (Intersector.overlaps(pilota.getLimits(), bloc.getLimits())) {
				blocs.removeValue(bloc, false);
				velocitatPilotaY *= -1;
			}
		}
	}
	
	private void gestioInputJugador(float delta) {
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			if (jugador.getX() > 0) {
				jugador.moures((Constants.ALT_PANTALLA*-1) * delta, 0);
			} else if (jugador.getX() < 20)
				jugador.setPosition(20, jugador.getY());
		}
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			if (jugador.getX() < Constants.ALT_PANTALLA - 40) {
				jugador.moures(Constants.ALT_PANTALLA * delta, 0);
			} else if (jugador.getX() > Constants.ALT_PANTALLA - 40)
				jugador.setPosition(Constants.ALT_PANTALLA-40, jugador.getY());
		}
	}
	
	@Override
	public void dispose() {
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}
}
