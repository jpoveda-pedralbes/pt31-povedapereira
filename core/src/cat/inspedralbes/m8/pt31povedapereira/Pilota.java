package cat.inspedralbes.m8.pt31povedapereira;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

public class Pilota {
	private Vector2 velocitat;
	private Sprite pilota;
	private Circle limits;
	
	public Pilota (float x, float y) {
		velocitat = new Vector2(0,0);
		pilota = new Sprite(Recursos.instance.getPilota());
		pilota.setPosition(x, y);
		limits = new Circle(pilota.getX(), pilota.getY(), Constants.AMPLE_PILOTA / 2);
	}
	
	public void actualitzarse (float delta) {
		pilota.translate(velocitat.x * delta, velocitat.y * delta);
		limits.setPosition(pilota.getX(), pilota.getY());
	}
	
	public void estableixVelocitat(float x, float y) {
		velocitat.x = x;
		velocitat.y = y;
	}
	
	public Circle getLimits() {
		return limits;
	}
	
	public void dibuixarse(SpriteBatch batch) {
		pilota.draw(batch);
	}
}
