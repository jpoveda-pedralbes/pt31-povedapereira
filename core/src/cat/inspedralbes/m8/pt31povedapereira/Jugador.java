package cat.inspedralbes.m8.pt31povedapereira;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Jugador {
	private Sprite jugador;
	private Rectangle limits;
	public Rectangle limit1, limit2, limit3;
	
	public Jugador (float x, float y) {
		init(x, y);
	}
	
	private void init(float x, float y) {
		jugador = new Sprite(Recursos.instance.getJugador());
		jugador.setPosition(x, y);
		limits = new Rectangle(jugador.getX(), jugador.getY(), Constants.AMPLE_JUGADOR, Constants.ALT_JUGADOR);
		limit1 = new Rectangle(jugador.getX(), jugador.getY(), 10, 5);
		limit2 = new Rectangle(jugador.getX() + 10, jugador.getY(), 20, 5);
		limit3 = new Rectangle(jugador.getX() + 30, jugador.getY(), 10, 5);
	}
	
	public void setPosition(float x, float y) {
		jugador.setPosition(x, y);
	}
	
	public Rectangle getBounds() {
		return limits;
	}
	
	public float getX() {
		return jugador.getX();
	}
	
	public float getY() {
		return jugador.getY();
	}
	
	public void actualitzarse (float delta) {
		limits.setPosition(jugador.getX(), jugador.getY());
		limit1.setPosition(jugador.getX(), jugador.getY());
		limit2.setPosition(jugador.getX() + 10, jugador.getY());
		limit3.setPosition(jugador.getX() + 30, jugador.getY());
	}
	
	public void moures (float x, float y) {
		jugador.translate(x, y);
	}
	
	public void dibuixarse(SpriteBatch batch) {
		jugador.draw(batch);
	}
}
