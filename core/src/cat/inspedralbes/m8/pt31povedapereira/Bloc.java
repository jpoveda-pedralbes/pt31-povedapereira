package cat.inspedralbes.m8.pt31povedapereira;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Bloc {
	private Sprite spriteBlock;
	private Rectangle bounds;
	
	
	public Bloc (float x, float y) {
		init(x, y);
	}
	
	private void init(float x, float y) {
		spriteBlock = new Sprite(Recursos.instance.getBloc());
		spriteBlock.setPosition(x, y);
		bounds = new Rectangle(spriteBlock.getX(), spriteBlock.getY(), 20, Constants.ALT_BLOC);
	}
	
	public Rectangle getLimits() {
		return bounds;
	}
	
	public void actualitzarse (float delta) {
		bounds.setPosition(spriteBlock.getX(), spriteBlock.getY());
	}
	
	public void dibuixarse(SpriteBatch batch) {
		spriteBlock.draw(batch);
	}
}